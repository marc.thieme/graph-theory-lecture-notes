#import "template/template.typ": *

#show: project.with("Graph Theory", "Prof. Dr. Maria Axenovich", "Marc Thieme")

#let w17(..tasks) = [WS17|#(tasks.pos().map(str).join(", "))]
#let w19(..tasks) = [WS19|#(tasks.pos().map(str).join(", "))]
#let s20(..tasks) = [SS20|#(tasks.pos().map(str).join(", "))]
#let w21(..tasks) = [WS21|#(tasks.pos().map(str).join(", "))]
#let s22(..tasks) = [SS22|#(tasks.pos().map(str).join(", "))]

= Theorems
#theorem(name: "Hall", exams: w19(4))[
  Let $G$ be a bipartite graph with partite sets $A$ and $B$. Then $G$ has a matching
  containing all vertices of $A$ if and only if $|N(S)|>=|S|$ for all $S subset.eq A$.
]
#theorem(name: "König", exams: (w19(5), w21(2)))[
  Let $G$ be bipartite. The size of the largest matching is 
  the same as the size of a smallest vertex cover.
]
#theorem(name: "Min degree path", exams: s20(2))[
  If a graph $G$ has minimum degree $delta(G) >= 2$, then $G$ has a path of length
  $delta(G)$ and a cycle with at least $delta(G) + 1$ vertices.
]
#lemma(name: "Extremal number for cycles", exams: w21(5))[
  Every graph $G$ with $|E(G)| > |G|$ contains a cycle.
]
#theorem(name: "Dirac", exams: w17(1, 7))[
  Every graph with $n>=3$ vertices and minimum degree 
  at least $n/2$ has a Hamiltonian cycle.
]
#theorem(name: "Tutte's", exams: w17(1))[
  A graph $G$ has a perfect matching 
  $q(G - S) <= |S| quad forall S subset.eq V$\
  where $q(G-S)$ denotes the number of odd components of $G - S$
]
#theorem(name: "Menger's", exams: w17(2))[
  For any graph $G$ and any two vertex sets $A,B subset.eq V(G)$,
  the smallest number of vertices separating $A$ and $B$ is 
  equal to the largest number of disjoint $A$-$B$-paths.
]
#theorem(name: "Global Menger's", exams: w17(2))[
  A graph $G$ is $k$-connected iff for any two vertices
   $a, b$ in $G$ there exist $k$ independent $a$-$b$-paths.
]
#corollary(exams: w17(2))[
  Let $G$ be a $k$-connected graph. Let $G'$ be a graph with $V(G') = V(G) union {v}$.
  Then $G'$ is $k$-connected iff $d_G' (v) >= k$.
]
#proof[
  Directly via the definition of connectivity. Take away $k$ edges and you see that it's still connected.
]
#corollary(name: "Fan Lemma", exams: s20(4))[
  If $k ≥ 1$ is an integer and G is a k-connected graph, then for every 
  $U subset V (G)$ with $|U| = k$ and every $x ∈ V (G) without U$ there exists a
  collection of $k$ paths from $x$ to $U$ that only have the vertex x in common.
]
#proof[
  If $G$ is $k$-connected, there exist the paths:\
  Given $x$ and $U$. Add a vertex $v$ to the graph with the $k$ vertices from $U$ as neighbours.
  The resulting graph is still $k$-connected.
  Following Menger's, there exist $k$ "independent" paths from $x$ to $v$.
   Also, they must run through $H$.

  If for all $U$ there are independent paths, $G$ is $k$-connected:\
  Let $U:=N(x)$. Now, complete the $k$ independent paths by appending $x$ respectively.
  You get $k$ "independent" paths to the vertices from $U$.
]
#theorem(name: "Kuratowski", exams: w17(6))[
  $G$ is planar iff
  1. $G$ does not contain $K_5$ or $K_(3,3)$ as minors or
  2. $G$ does not contain $K_5$ or $K_(3,3)$ as topological minors
]
#corollary(name: "Outerplanar", exams: w19(2))[
  An outerplanar graph does not contain $K_4$ as a minor.
]
#proof[
  Assume there exists an outerplanar graph $G$ which contains $K_4$ as a minor.
  Consider its planar embedding $E$. In the outer face of $E$, add a vertex $v$.
  We can connect $v$ to all other vertices and still have a plane embedding.
  However, the new graph now has $K_5$ as a minor by treating ${v}$ as 
  one of the vertex sets. A contradiction.
]
#theorem(name: "Planar Euler", exams: [_every exam_])[
  For a connected plane graph: $n - m + f = 2$
]
#theorem(name: "Triangulation edges", exams: w17(6))[
  A plane graph with $n>=3$ vertices has at most $3n-6$ edges.
   Every plane triangulation has exactly $3n-6$ edges.
]
#theorem(name: "Triangle-free-plane graph edges", exams: w17(6))[
  A triangle-free plane graph with $n>=3$ vertices has at most $2n - 4$ edges.
]
#theorem(name: "Eulerian Tour", exams: w17(7))[
  A connected graph has an eulerian tour iff all vertex degrees are even.
]
#theorem(exams: w17(7))[
  For any graph $G$, either $G$ or $overline(G)$ is connected.
]
#corollary(exams: w17(7))[
  Let $G$ be a $k$-regular graph. If $k$ is odd, then $|G|$ is even.
]
#proof[
  Assume $k$ and $n:=|G|$ are both odd. Then, $||G||=(n k)/2$.
  However, since both $n$ and $k$ are odd,
   $n k$ is odd and $||G|| = (n k)/2 in.not NN$.
  A contradiction.
]
#lemma(name: "Connected Graph Vertex Ordering", exams: w17(3))[
  For any connected graph $G$ and for any vertex $V$ there is an ordering 
  of the verticres of $G: v_1, ..., v_n$ such that $v=v_n$ and for each 
  $i, 1<=i<n$, $v_i$ has a higher indexed neighbour.
] <ordering-lemma>
#proof[
Consider a spanning tree T of G and create a sequence of sets $X_1, ..., X_(n−1)$
with $X_1 = V(G), X_i = X_(i−1) − v_(i−1)$, where vi is a leaf of $T[X_i]$ not equal to v, for
$i = 2, ..., n−1$. Then $v_1, ..., v_n$ is a desired ordering.
]
#theorem(name: "Brook's", exams: (w17(3), w21(4)))[
  Let $G$ be a connected graph. Then $chi(G) <= Delta(G)$ unless $G$
   is a complete graph or an odd cycle.
]
#theorem(name: "Turan", exams: w17(5))[
  $"ex"(n, k) = t_(k-1)(n)$
]
#theorem(name: "Classic Ramsey bound", exams: s22(7))[
  $root(k, 2) <= R(k) <= 4^k$
]
#proof[
_For the upper bound_, consider an edge-coloring of $G = K_(4k)$ with colors red and
blue. Construct a sequence of vertices $x_1, . . . , x_(2k)$, a sequence vertex sets $X_1, . . . , X_(2k)$,
and a sequence of colors $c_1, . . . , c_(2k−1)$ as follows. Let $x_1$ be an arbitrary vertex, $X_1 =
V(G)$. Let $X_2$ be the largest monochromatic neighborhood of $x_1$ in $X_1$, i.e., largest
subset of vertices from $X_1$, such that all edges from this subset to $x_1$ have the same
color. Call this color $c_1$. We see that $|X_2| ≥ ceil((|X_1| - 1)/2) ≥ 4^k/2$. Let $x_2$ be an arbitrary
vertex in $X_2$. Let $X_3$ be the largest monochromatic neighborhood of $x_2$ in $X_2$ with
respective edges of color $c_2, x_3 in X_3$, and so on let $X_m$ be the largest monochromatic
neighborhood of $x_(m−1)$ with respective color $c_(m−1)$ in $X_(m−1)$, $x_m ∈ X_m$. We see that
$|X_m| ≥ 4^k/2^(m−1)$. Thus $|X_m| > 0$ as long as $2k > m−1$, i.e., as long as $m ≤ 2k$.
Consider vertices $x_1, ..., x_(2k)$ and colors $c_1, ..., c_(2k−1)$. At least k of the colors, say
$c_(i_1), c_(i_2), ..., c_(i_k)$ are the same by pigeonhole principle, say without loss of generality,
red. Then $x_(i_1), x_(i_2), ..., x_(i_k)$ induce a k-vertex clique all of whose edges are red.
]

// #corollary(name: "Turan Bound", exams: w17(5))[
//   $t_k(n) <= ceil((n^2 (k-1))/(2k))$
// ]
// #proof[
//   Let $T:=T_k(n)$. nWe know $delta(T) = Delta(T) - 1$. 
//   Let $g$ be the number of vertices with maximum degree. Then we calculate
//   $
//     t_k(n) = 1/2n dot delta(T) + 1/2 g 
//     = 1/2n(n-ceil(n/(r-1))) + 1/2k
//     = 1/2n dot floor(n (r-2)/(r-1)) + 1/2 k
//   $
// ]

#lemma(exams: w17(5))[
  Let $H subset G$. Then $"ex"(n, H) <= "ex"(n, G)$.
]
#theorem(name: "Ear decomposition", exams: w19(1))[
  A graph $G$ is 2-connected iff it has an ear decomposition
   starting from any cycle in $G$.
]
#theorem(name: "Mader's Theorem", exams: w21(5))[
  Every graph with average degree at least $4k$ has a $k$-connected subgraph.
]
#theorem(name: "König", exams: w19(5))[
  If $G$ is a bipartite graph, then $chi'(G) = Delta(G)$
]
#lemma(name: "Edge colorings are bipartite matchings", exams: w21(3))[
  In a proper edge coloring of a graph, the partitions of each color form a matching.
]
#theorem(name: "Erdös-Renyi", exams: w19(6))[
Let $H$ be a balanced graph with at least one edge. Then $t(n)=n^(-1/epsilon(H))$
is a threshold function for a property $P_H={G:H subset.eq G}$ \
where $epsilon(F) = (||F||)/(|F|)$
]

= Vorausgesetzt
#theorem(name: "Pigeonhole Principle", exams: w17(6))[
  When $m$ items are put into $n$ containers with $m>n$,
   then at least one container receives more than one item.
]
#theorem(name: "Probability", exams: s20(6))[
  $
    PP(A sect B) = PP(A) + PP(B) - PP(A union B) = PP(A|B) dot PP(B)
  $
]
#theorem(name: "Linearity of expectation", exams: w21(7))[
  $E[alpha X + Y] = alpha E[X] + E[Y]$
]
#theorem(name: "Max of random variables", exams: w19(6))[
  $PP(max{X, Y} < c) = PP(X < c, Y < c)$
]

= Definitions
#definition(name: "L-list-colorable", exams: w17(3))[
  Let $L(v) subset.eq NN$ be a list of colors for each vertex $v in V$. We say
  that $G$ is _L-list-colorable_ if there is a coloring $c : V -> NN$ such that
  $c(v) in L(v)$ for each $v in V$ and adjacent vertices receive different colors.
]
#definition(name: "k-list-colorable", exams: w17(3))[
  Let $k in NN$. We say that $G$ is _k-list-colorable_ if $G$ is _L-list-colorable_
  for each list $L$ with $|L(v)| = k quad forall_(v in V)$.
]
#definition(name: "Eulerian tour", exams: w17(7))[
An _eulerian_ tour is a closed walk that traverses every edge exactly once.
]
#definition(name: "Eulerian", exams: w17(7))[
  A graph is _eulerian_ if it contains an eulerian tour.
]
#definition(name: "Connectivity", exams: s20(4))[
  The maximum $k$ for which $G$ is connected is called its
  connectivity $kappa(G)$.
]
#definition(name: "Ear Decomposition", exams: s22(6))[
  An _ear-decomposition_ of a graph is a sequence $G_0 subset.eq G_1 subset.eq ... subset.eq G_k$
  of graphs, such that
#set align(left)
- $G_0$ is a cycle
- for each $i=1, ..., k$ the graph $G_i = G_(i-1) union P_i$, where $P_i$ is a $G_(i-1)$-path,
 i.e., $P_i$ is an ear of $G_i$ and
- $G_k = G$.
]
#definition(name: "Co-Clique/Independence Number", exams: w21(2))[
  The _co-clique_ number $alpha(G)$ is the size of the largest independent set in $G$.
]
#definition(name: "Clique number", exams: w21(2))[
  The _clique_ number $omega(G)$ is the order of the largest clique in $G$.
]
#definition(name: "Perfect graph")[
  A graph $G$ is called _perfect_ if $forall_(H underbrace(subset.eq, "induced") G) quad chi(H) = omega(H)$
]
#definition(name: "Almost all graphs", exams: w19(6))[
  A property propertyP is a set of graphs, e.g. P = {G : G is k-connected}.
Let (pn) ∈ [0, 1]N be a sequence. We say that G ∈ G(n, pn) almost always almost alwayshas
property P if Prob(G ∈ G(n, pn
) ∩ P) → 1 for n → ∞. If (pn) is constant p, we
also say in this case that almost all graphs in G(n, p) have property P
]

= Ansätze
#remark(name: "Induction on trees", exams: s20(1))[
  On tasks with trees, do induction.
]
#remark(name: "Coloring construction with ordering")[
_When proving coloring results with respect to the max. vertex degree_.\
  Use the @ordering-lemma. For any vertex, take an ordering such that each vertex
  has a neighbour of higher order. Now, greedily color the vertices in some way.
]
#remark(name: "Coloring construction with max function", exams: s20(7))[
  _Let G be a graph. Prove that χ(G) ≤ 1 + max{δ(H) : H ⊆ G}_
  Construct a greedy coloring (by coloring the vertices in order smallest degrees) and in each step,
  find a subgraph such that its smallest degree is larger than the number of colors used.
]
#remark(name: "Ramsey bipartite graph construction", exams: w17(6))[
  _Prove that $underbracket(H subset K_(n,n), "monochromatic")$_.\
First observe that there $k>=1/2n$ vertices who have $g>=1/2n$ neighbours of one color.
Then use the pigeonwhole principle or a variation to prove that there exists some structure.
]
#remark(name: "Proof of chromatic number", exams: w19(3))[
  Induktion über die Knotenanzahl.
  1-connected ausschließen, weil dann geht es einfach über die Blöcke.
]
#remark(exams: w19(3))[
  A permutation on $n$ elements is well expressed as a perfect matching on $K_(n,n)$.
]
#remark(exams: w19(3))[
  Gar nicht erst anfangen blind zu rechnen und zu hoffen, dass
  etwas nützliches rauskommt. Immer vor Augen haben, was bei 
  rauskommen muss.
]
#remark(name: "Ramsey numbers", exams: w19(7))[
  Remeber its only an edge coloring. Don't get all caught up in trying to find a 
  structure when you accidentily constructed a graph where the vertices are colored.
]

= Aufgaben
#example(name: "Chromatic Number, Planar Graph with girth 6", exams: w19(3))[
Let G be a planar graph of girth at least 6. Prove that the chromatic number of G
is at most three
]
#proof[
We shall use induction on |V (G)|, with trivial base cases |V (G)| = 1, 2. Now, let G
be a planar graph of girth at least 6 with n := |V (G)| > 2, and suppose the result holds
for smaller values of n. We may assume that G is 2-connected: otherwise, we apply the
induction hypothesis to the blocks of G. We claim that G contains a vertex of degree at
most 2. Indeed, suppose that G has m edges, and $l$ faces and observe that
$
2m = ∑_(i∈N) i · f_i
$
where fi denotes the number of faces with precisely i edges in their boundary. This holds
since G is 2-connected, and therefore every edge lies on the boundary of precisely two
faces (a result from lecture). Since G has girth 6, we note that ∑
i∈N i · fi ≥ 6$l$, and
therefore $l$ ≤ m/3.\
Now, Euler’s formula implies that 2 = n − m + $l$ ≤ n − 2m/3, and so we obtain
$
m ≤ 3n/2 − 3.
$Since ∑
v∈V (G) deg(v) = 2m < 3n, there must exist a vertex of degree at most 2.
Let v be a vertex of degree at most two. Then G − v is still planar with girth at least
6, so by induction there is a 3-coloring of G − v. Giving v a color not appearing in its
neighborhood, we obtain a 3-coloring of G, completing the proof.
]

#example(name: "Fan Lemma", exams: s20(2))[
 One particular consequence of Menger’s theorem is the fan lemma: If k ≥ 1 is an integer and G is
a k-connected graph, then for every U ⊂ V (G) with |U | = k and every x ∈ V (G) \ U there exists a
collection of k paths from x to U that only have the vertex x in common.
Use the fan lemma to prove that for every integer k ≥ 2, any k-connected graph of order at least 2k
contains a cycle of length at least 2k. 
]
#proof[
  Consider a longest cycle C. If V (C) = V (G), then we are done since |V (G)| ≥ 2k by assumption.
Otherwise, we may choose a vertex x ∈ V (G) \ V (C). Since G is k-connected, every vertex of G has
degree at least k ≥ 2. In particular, this implies that G contains a cycle of length at least k + 1 (by
considering a longest path). Thus, |V (C)| ≥ k + 1.
We now aim to apply the fan lemma. Pick a subset U ⊂ V (C) of size k. The fan lemma implies that
there are k paths P1, . . . , Pk from x to U that only have the vertex x in common. For each path Pi, let
si denote the first vertex in Pi that intersects V (C). In particular, the vertices s1, . . . , sk are pairwise
distinct. Without loss of generality, assume they are arranged as s1, s2, . . . , sk clockwise along C.
We claim that si and si+1 are not consecutive on C for any i = 1, . . . , k (computed modulo k). Indeed,
fix vertices si and si+1. If they are consecutive along C, then the cycle xPi+1si+1CsiPix (following C
clockwise) is longer than C, a contradiction. It follows that there is at least one vertex between si and
si+1, and hence |V (C)| ≥ 2k as required.
]

#example(name: [Matching w.r.t. $alpha(G)$], exams: w21(2))[
  Let G be a graph on n vertices such that α(G) = n 2 and α(H) ≥ |V (H)| 2 for any subgraph H of G. 
  Prove that G has a 1-factor (perfect matching). 
  Here, α(G) is the number of vertices in a largest independent set of G
]
*My solution:*
#proof[
  Let $I$ be a maximal independent set in $G$. We know $alpha(G) = (|G|)/2$.
  We also know that $G[I] = (|G|)/2 dot K_1$.
  We define the bipartite auxilary graph $M := (A union.dot I, E)$ 
  where $A = V(G) without I, quad E := E(G) sect A times I$.
  Since $V(M) = A union.dot B = V(G) without I union I = V(G)$ and $E(M) subset.eq E(G)$, a 
  1-factor of $M$ also constitutes a 1-factor of $G$.

  Since $|A| = |I|$, per Hall's theorem, $M$ contains a perfect matching iff $quad forall_(S subset.eq A) |N(S)| >= |S|$.

  Assume $quad exists_(S subset.eq A) |N(S)| < |S|$.

  We know look at $H:=G[S union.dot N(S)]$. Since $alpha(H) >= (|H|)/2$, there exists an independent set $J$ on $H$
  with $|J| >= (|H|)/2$. We take a step back and look at $I' = I without N(S) union J$. 
  Now $I without N(S)$ and $J$ are both independent matchings. Also, since $J sect A subset.eq S$ doesn't have neighbours in
  $I without N(S)$ and $J sect I subset.eq I$ is not adjacent to any vertex in $I$, $I'$ is an independent matching.
  Since $|J| >= (|H|)/2 = (|S| + overbrace(|N(S)|, <|S|)|)/2 > |N(S)|$, $|I'| = |I without N(S) union.dot J| > |I|$.
  Therefore, we have shown that there exists a matching $I'$ with $|I'| > alpha(G)/2$. A contradiction.
]
*Official solution:*
#proof[
  Claim: G is bipartite. \
  Assume not. Then G contains an odd cycle C. We have α(C) < |C| 2 , a contradiction.
  Thus, G must be bipartite. Now assume there is no perfect matching in G.
  By K¨onig’s theorem, the size of a largest matching equals the size of a smallest vertex cover in G, so there is a vertex cover A of size < $n_2$ .
  Then clearly the set B = V (G) $without$ A induces no edge, B is an independent set of size $> n − n_2 = n_2$ , a contradiction
]

= Abgefragte Sätze
#theorem(name: "König's for edge coloring")[
Induction on _edge_ count.\
Take out an edge $x y in E(G))$ and color the remaining graph.
1. If $"Mis"(x) sect "Mis"(y) = emptyset$, trivial.
2. Else, let $alpha in "Mis"(x), beta in "Mis"(y)$ and consider the longest $alpha, beta$ colored path starting from $x$
]
#proof[
Let $alpha in "Mis"(x), beta in "Mis"(y)$ and consider the longest $alpha, beta$ colored path $P$ starting from $x$.
Because of parity, $P$ does not end in $y$ and because $beta$ is not incident to $y$, $y in.not P$. 
Switch colors $alpha$ and $beta$ in $P$. We obtain a proper edge coloring with $beta$ in $"Mis"(x) sect "Mis"(y)$.
Now, we can color $x y$ in $beta$ and have a proper coloring in $G$ with $Delta(G)$ colors.
]
#theorem(name: "Dirac")[
1. Proof that $G$ is connected (easy).
2. Let $P$ be a longest path in $G$. Proof that $V(P)$ induces a cycle.
3. Show that if $|P|<n$, we can construct a longer path based on the cycle from before.
]
#proof[
\
*_Claim 1_*: Assume $G$ is disconnected. Let $C_1, C_2$ be components of $G$. Let $x in C_1, y in C_2$.
Since $|C_1| >= |N(x) union.dot {x}| > |N(x)| >= n/2$ and $C_1 sect C_2 = emptyset$, $|C_2|< n/2$. 
However, then $N(y) subset.eq V(C_2) without {y}$ and therefore $|N(y)| < n/2 <= delta(G)$, a contradiction.

*_Claim 2_*: Let $P={v_0, ..., v_k}$ be a longest path in $G$. If $v_0 v_k in E(G)$, $P v_0$ induces a cycle in $G$ and we are done.
Else let $E_0 = v_0 times N(v_0)$ and $E_k = v_k times N(v_k)$. Since $v_0 v_k in.not E(G), quad E_0 sect E_k = emptyset$.\
Let $(i in [k]-{0,k}) quad Q_i = {v_(i+1) v_0, v_i v_k} sect E(G)$. Since $P$ is a largest path, each neighbour of $v_0$ and $v_k$ lies in $P$.
Therefore, $union_(i in [k]-{0,k}) Q_i = E_0 union E_k$. Also $Q_i sect Q_j = emptyset quad (i, j in [k]-{0,k})$. Therefore,
$sum_(i in [k]-{0,k}) |Q_i| = |E_0| + |E_k| = 2 n/2 = n$. From the pigeonhole principle follows that $exists_i: |Q_i| >= 2$. Observe now
that $v_0 P v_i v_k P v_i+1 v_0$ is a cycle in $G$, induced by $V(P)$.

*_Claim 3_*: Let $C$ be a largest cycle in $G$. Since $delta(G) >= n/2$, $|C| >= n/2+1$ in $G$. Therefore, each vertex $v$ not in $C$ has
at least two neighbours $a, b in C$. However, then $C - a b + a x + x b$ is a cycle in $G$. Therefore, there does not exist a vertex not in $C$.
]
#theorem(name: "Ear decomposition")[
1. *Ear decomposition $=>$ 2-connected*: Straightforward.
2. *2-connected $=>$ Ear decomposition*:
]
#proof[
*Ear decomposition $=>$ 2-connected*: Induction on length of Ear-decomposition.\
*2-connected $=>$ Ear decomposition*: Let $H$ be a largest subgraph obtained by an ear-decomposition $G_0 subset.eq ... subset.eq G_k$
 where $G_0$ is a cycle. Assume $H != G$. We see that $H$ is an induced subgraph of $G$ as otherwise, an edge which is not in $H$ but
whose vertices are could be added as an ear.\
Since $G$ is connected, there exists an edge $a b in E(G)$ such that $a in.not H$ and $b in H$.
Let $w in H-b$. Since $G$ is 2-connected, there exists a path $P$ between $a$ and $w$ in $G-b$.
However, note that $w P a b$ is an ear in $H union w P a b$. This contradicts the maximality of $H$. Therefore, $H = G$
]
#theorem(name: "Hall's theorem")[
Let $G = (A union.dot B, E(G))$. We do induction on $|A|$.
1. *Has matching $=>$ Satisfies Hall's condition*: Straightforward.
2. *Has matching $=>$ Satisfies Hall's condition*: Induction on the number of vertices in $A$.
 Distinguish cases $|N(S)| > |S|$ and $|N(S)| >= |S|$ (which uses $|N(S)| > |S|$).
]
#proof[
*Has matching $=>$ Satisfies Hall's condition*:\
_Case 1_: $|N(S)|>=|S|+1$ for all $S subset.eq A, S in.not {A, emptyset}$.\
Let $G'=G - {x, y}$ for $x y in E(G)$ with $x in A, y in B$.
 Now, Hall's condition holds for $G'$ and a matching $M$ which contains all vertices in $A$ exists.
 Since $x, y in.not V(G') = V(M)$, $M + x y$ is a matching in $G$ which contains all vertices from $A$.\
_Case 2:_ $|N(S_1)| = |S_1|$ for an $S_1 subset.eq A$. Let $A' = S_1$ and $B' = N(S_1)$. Since $|A'| < |A|$
and because Hall's condition still holds, per induction step, there exists a matching $M'$ on $A union.dot B$
which includes all vertices of $A$.\
We claim that $A - S_1 union.dot B - N(S_1)$ also satisfies Hall's condition. Assume not. Then there is $S in A'$
such that $|N_A' (S)| < |S|$. Then $|N(S union A')| = |B' union N_G''$... Ich geb auf.\
|NG(S ∪ A′)| = |B′ ∪ NG′′ (S)| = |B′| + |NG′′ (S)| < |A′| + |S| = |A′ ∪ S|.\
$==>$ Zeigen, dass $G'-A'$ auch Halls' Condition erfüllt durch:
$
  A'' = A - A', quad B'' = B - B'\
  S'' in A'' "such that" |N_G''(S)| < |S|\
  |N_G (S'' union A')| = |N_G'' (S'') union B'| = |N_G'' (S'')| + |B'| < |S''| + |A'| < |A' union S''|
$
]
#theorem(name: "Ramsey")[
  $R(k) <= l^(l k)$ with $l$ colors.
]
#proof[
  Assume an edge coloring of $K_(l^(l k)$
  For some color $c$, let $N_c (x in V(G))$ denote those neighbours of $x$, such that the edge connecting
  them has color $c$.
  We will find series $x_1, ..., x_(l k)$, $X_1, ..., X_(l k)$ and $c_1, ..., c_(l k)$ for our construction.\
  Let $X_1 := V(G)$. Let $x_1 in X_1$ arbitrarily. Choose color $c_1$ such that $|N_c_1 (x_1)|$ is maximal.
  Let $X_2 := N_c_1 (x_1)$. Note that $|X_2| >= n/l$ per pigeonhole principle.\
  Now let $x_2 in X_2$ arbitrarily. Choose $c_2$ such that $|N_c_2 (x_2) sect X_2|$ is maximal.
  Let $X_3 := |N_c_2 (x_2) sect X_2|$. Note that $|X_3| >= |N_c_2 (x_2) sect X_2| >= |X_2|/l >= n/(l^2)$ Continue like that.\
  Note that $X_i >= n/(l^i)$. Since $X_(l k) = n/(l^(l k)) = 1$, we know that $X_1, ..., X_(l k)$ are all larger than 1.\
  Per pigeonhole principle, we can pick some color $z$ such that $c_j_1, ..., c_j_k (j_s != j_t quad forall_(s!=t))$ have the same color.\
  Since $forall_(1 <= p < q <= k) quad X_j_q subset X_(j_p-1) subset ... subset X_j_p$ and therefore $x_j_q in N_c_j_p (x_j_p)$,
  $quad x_j_1, ..., x_j_k$ induce a monochromatic $K_k$.
]
#theorem(name: "Plnanar 5-colorability")[]
#proof[
  Assume that $G$ is a plane triangulation. If not, we could add edges until it is and a proper 5-coloring would also be proper in $G$.
  Induction on $n=|G|$.\
  For $|G| <= 5$, the statement holds trivially as every vertex can be colored in a different color.\
  Assume the statement holds for an $n in NN$. Let $G$ be a graph of order $n+1$.\
  Because $G$ is maximally plane, $||G|| <= 3n - 6$. As such, average degree $d(G) <= 2 (||G||)/n <= 6 - 12/n$. Therefore, there
  must exist at least one vertex $x$ with $d(x) <= 5$. Per induction assumption, $G-x$ has a proper 5-coloring $c$.\
  Let $N(x):=N_G (x)$. If all $|{c(v) : v in N(x)}| < 5$ we can simply color $x$ in a color which does not occur in $N(x)$.
  Assume that all neighbours have pairwise different colors. Name ${v_1, ..., v_5}:= N(x)$ such that they are cyclically arranged.
  Let $c_1:=c(v_1), ..., c_5:=c(v_5)$. Perform a $c_1 c_3$ switch at $v_1$. So switch $c(v_1)$ to $c_3$ and greedily switch all 
  subsequently adjacent occurences of the two colors. If this switch also switches the color of $v_3$, then there must exist a path $P_1$
  colored in $c_1$ and $c_2$ to $v_3$. If it does not switch $c(v_3)$, we are done because we can now color $x$ in $c_1$. 
  If it does change its color, we perform a $c_2 c_4$ switch on $v_2$. If this also switches $c(v_4)$, then there must also exist
  a path $P_2$ colored only $c_2 and c_4$ from $v_2$ to $v_4$. However, now $P_1$ and $P_2$ must intersect. Their edges can't cross due to
  planarity but they also can't share a vertex because their colors ${c_1, c_3} sect {c_2, c_4} = emptyset$. Therefore, such a path can't
  exist and the $c_2 c_4$ switch cannot propagate to $v_4$. Now we can color $x$ in $c_2$ and are done.
]
